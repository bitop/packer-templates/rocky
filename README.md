# <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Rocky_Linux_wordmark.svg/1600px-Rocky_Linux_wordmark.svg.png" alt="drawing" width="512"/>

## Table of Contents

* [Introduction](#introduction)
* [Requirements](#requirments)
* [Configuration](#configuration)
* [Build](#build)

## TL;DR

## Introduction

This repository provides infrastructure-as-code examples to automate the creation of virtual machine images and their guest operating systems on VMware vSphere using [HashiCorp Packer][packer] and the [Packer Plugin for VMware vSphere][packer-plugin-vsphere] (vsphere-iso). All examples are authored in the HashiCorp Configuration Language ("HCL2").

By default, the machine image artifacts are turned into templates with in vsphere.  You will need to provide a folder where you want templates to be stored.

## Requirements

**Packer**:
* HashiCorp [Packer][packer-install] 1.7.7 or higher.
* HashiCorp [Packer Plugin for VMware vSphere][packer-plugin-vsphere] (`vsphere-iso`) 1.0.2 or higher.

    >Required plugins are automatically downloaded durring the `packer init` phase.  These plugins are placed in the same directory as your Packer executable `/usr/local/bin` or `$HOME/.packer.d/plugins`.

**Additional Software Packages**:

The following software packages must be installed on the Packer host:

* [Git][download-git] command line tools.
  - Ubuntu: `apt-get install git`
  - RHEL/Rocky: `dnf install git`
  - macOS: `brew install git`
  - Windows: `choco install git`
* [Ansible][ansible-docs] 2.9 or higher.
  - Ubuntu: `apt-get install ansible`
  - RHEL/Rocky: `dnf install ansible`
  - macOS: `brew install ansible`
* A command-line .iso creator. Packer will use one of the following:
  - **xorriso** on Ubuntu: `apt-get install xorriso`
  - **mkisofs** on Ubuntu: `apt-get install mkisofs`
  - **hdiutil** on macOS: native
* mkpasswd
  - Ubuntu: `apt-get install whois`
  - RHEL/Rocky: `dnf install whois`
  - macOS: `brew install --cask docker`
* Coreutils 
  - macOS: `brew install coreutils`

**Platform**:
* VMware vSphere 7.0 Update 2 or higher

## Configuration

### Step 1 - Download the release

Download the **latest** release

The directory structure of the repository.

```
┣ ansible/
┃ ┣ roles/
┃ ┃ ┣ <role>/
┃ ┃ ┃ ┣ defaults/
┃ ┃ ┃ ┃ ┗ main.yaml
┃ ┃ ┃ ┣ files/
┃ ┃ ┃ ┃ ┗ .gitkeep
┃ ┃ ┃ ┣ handlers/
┃ ┃ ┃ ┃ ┗ main.yaml
┃ ┃ ┃ ┣ meta/
┃ ┃ ┃ ┃ ┗ main.yaml
┃ ┃ ┃ ┣ tasks/
┃ ┃ ┃ ┃ ┣ main.yaml
┃ ┃ ┃ ┃ ┗ *.yaml
┃ ┃ ┗ ┗ vars/
┃ ┃     ┗ main.yaml
┃ ┣ ansible.cfg
┃ ┗ main.yaml
┣ builds/
┃ ┣ common/
┃ ┃ ┣ build.pkrvars.hcl
┃ ┃ ┣ common.pkrvars.hcl
┃ ┃ ┗ vsphere.pkrvars.hcl
┃ ┗ os/
┃   ┗ <distribution-version>/
┃     ┣ data/
┃     ┃ ┣ meta-data
┃     ┃ ┗ user-data.pkrtpl.hcl
┃     ┣ *.auto.pkrvars.hcl
┃     ┣ *.pkr.hcl
┃     ┗ variables.pkr.hcl
┣ scripts/
┃ ┗ *.sh
┣ .gitignore
┣ LICENSE
┣ README.md
┗ build_*.sh
```

The files are distributed in the following directories.
* **`ansible`** - contains the Ansible roles to initialize and prepare the machine image build.
* **`builds`** - contains the templates, variables, and configuration files for the machine image build.
* **`scripts`** - contains the scripts to initialize and prepare the machine image build.
* **`manifests`** - manifests created after the completion of the machine image build.

> **NOTE**: The project is transitioning to use Ansible instead of scripts, where possible.

### Step 2 - Download the Guest Operating Systems ISOs

1. Download the x64 guest operating system [.iso][iso] images.

    **Linux Distributions**
    * Ubuntu Server 20.04 LTS
        * [Download][download-linux-ubuntu-server-20-04-lts] the latest **LIVE** release `.iso` image. (_e.g._ `ubuntu-20.04.2-live-server-amd64.iso`)
    * Red Hat Enterprise Linux 8 Server
        * [Download][download-linux-redhat-server-8] the latest release of the **FULL** `.iso` image. (_e.g._ `rhel-8-x86_64-dvd1.iso`)
    * Red Hat Enterprise Linux 7 Server
        * [Download][download-linux-redhat-server-7] the latest release of the **FULL** `.iso` image. (_e.g._ `rhel-server-7-x86_64-dvd1.iso`)
    * AlmaLinux 8
        * [Download][download-linux-almalinux-server-8] the latest release of the **FULL** `.iso` image. (_e.g._ `AlmaLinux-8-x86_64-dvd1.iso`)
    * Rocky Linux 8
        * [Download][download-linux-rocky-server-8] the latest release of the **FULL** `.iso` image. (_e.g._ `Rocky-8-x86_64-dvd1.iso`)

    **Microsoft Windows**
    * Microsoft Windows Server 2022
    * Microsoft Windows Server 2019

    >**NOTE**: Please check if the iso's are already available in your vSphere environment.

2. Obtain the checksum type (_e.g._ `sha256`, `md5`, etc.) and checksum value for each guest operating system `.iso` image. This will be used in the build input variables.

3. [Upload][vsphere-upload] your guest operating system `.iso` images to the ISO datastore and paths that will be used in your variables.

    **Example**: `builds/common/common.pkvars.hcl`
    ```
    common_iso_datastore = "NFS-ISO"
    ```

    **Example**: `builds/os/<distribution-version>/*.auto.pkvars.hcl`
    ```
    iso_path           = "rocky"
    iso_file           = "rocky-8.5.iso"
    iso_checksum_type  = "md5"
    iso_checksum_value = "d8c4bc561e68afaf7815518f78a5b4ab"

### Step 3 - Configure the Variables

The [variables][packer-variables] are defined in `.pkvars.hcl` files.

#### **Build Variables**

Edit the `build/common/build.pkvars.hcl` file to configure the following:

* Credentials for the default account on machine images.

**Example**: `build/common/build.pkvars.hcl`

```
build_username           = "builder"
build_password           = "<plaintext_password>"
build_password_encrypted = "<sha512_encrypted_password>"
```

Generate a SHA-512 encrypted password for the  _`build_password_encrypted`_ using tools like mkpasswd.

**Example**: mkpasswd using Docker:

```
user@machine>  docker run -it --rm alpine:latestvmwar mkpasswd -m sha512
Password: ***************
[password hash]
```

**Example**: mkpasswd on Linux:

```
user@machine>  mkpasswd -m sha-512
Password: ***************
[password hash]
```

#### **Common Variables**

Edit the `builds/common/common.pkvars.hcl` file to configure the following common variables:

* Virtual Machine Settings
* Template and Content Library Settings
* Removable Media Settings
* Boot and Provisioning Settings

**Example**: `builds/common/common.pkvars.hcl`

```
// Virtual Machine Settings
common_vm_version           = 19
common_tools_upgrade_policy = true
common_remove_cdrom         = true

// Template and Content Library Settings
common_template_conversion     = false
common_content_library_name    = "Packer-Templates"
common_content_library_ovf     = true
common_content_library_destroy = true

// Removable Media Settings
common_iso_datastore = "NFS-ISO"

// Boot and Provisioning Settings
common_data_source      = "http"
common_http_ip          = null
common_http_port_min    = 8000
common_http_port_max    = 8099
common_ip_wait_timeout  = "20m"
common_shutdown_timeout = "15m"
```

##### Data Source Options

`http` is the default provisioning data source for Linux machine image builds.

You can change the `common_data_source` from `http` to `disk` to build supported Linux machine images without the need to use Packer's HTTP server. This is useful for environments that may not be able to route back to the system from which Packer is running. The `cd_content` option is used when selecting `disk` unless the distribution does not support a secondary CD-ROM. For distributions that do not support a secondary CD-ROM the `floppy_content` option is used.

```
common_data_source = "disk"
```

##### HTTP Binding

If you need to define a specific IPv4 address from your host for Packer's HTTP Server, modify the `common_http_ip` variable from `null` to a `string` value that matches an IP address on your Packer host. For example:

```
common_http_ip = "172.16.11.254"
```

#### **vSphere Variables**

Edit the `builds/common/vsphere.pkvars.hcl` file to configure the following:

* vSphere Endpoint and Credentials
* vSphere Settings

**Example**: `builds/common/vsphere.pkvars.hcl`

```
vsphere_endpoint             = "vsphere.server.local"
vsphere_username             = "svc-packer-vsphere@server.local"
vsphere_password             = "<plaintext_password>"
vsphere_insecure_connection  = true
vsphere_datacenter           = "dc-01"
vsphere_cluster              = "cl-01"
vsphere_datastore            = "ds-01"
vsphere_network              = "network-dhcp"
vsphere_folder               = "templates"
```

#### **Machine Image Variables**

Edit the `*.auto.pkvars.hcl` file in each `builds/os/<distribution-version>` folder to configure the following virtual machine hardware settings, as required:

* CPU Sockets `(init)`
* CPU Cores `(init)`
* Memory in MB `(init)`
* Primary Disk in MB `(init)`
* .iso Path `(string)`
* .iso File `(string)`
* .iso Checksum Type `(string)`
* .iso Checksum Value `(string)`

    >**Note**: All `variables.auto.pkvars.hcl` default to using the [VMware Paravirtual SCSI controller][vmware-pvscsi] and the [VMXNET 3][vmware-vmxnet3] network card device types.

#### **Using Environmental Variables**

Some of the variables may include sensitive information and environmental data that you would prefer not to save to clear text files. You can add these to environmental variables using the example below:

```
export PKR_VAR_build_username="<build_username>"
export PKR_VAR_build_password="<build_password>"
export PKR_VAR_build_password_encrypted="<build_password_encrypted>"
export PKR_VAR_vsphere_endpoint="<vsphere_endpoint_fqdn>"
export PKR_VAR_vsphere_username="<vsphere_username>"
export PKR_VAR_vsphere_password="<vsphere_password>"
export PKR_VAR_vsphere_datacenter="<vsphere_datacenter>>"
export PKR_VAR_vsphere_cluster="<vsphere_cluster>"
export PKR_VAR_vsphere_datastore="<vsphere_datastore>>"
export PKR_VAR_vsphere_network="<vsphere_network>"
export PKR_VAR_vsphere_folder="<vsphere_folder>"
```

## Step 5 - Modify the Configurations and Scripts (Optional)

If required, modify the configuration and scripts files, for the Linux distributions and Microsoft Windows.

### Linux Distribution Kickstart and Scripts

Username and password variables are passed into the kickstart or cloud-init files for each Linux distribution as Packer template files (`.pkrtpl.hcl`) to generate these on-demand.

### Microsoft Windows Unattended amd Scripts

Variables are passed into the [Microsoft Windows][microsoft-windows-unattend] unattend files (`autounattend.xml`) as Packer template files (`autounattend.pkrtpl.hcl`) to generate these on-demand.

By default, each unattended file set the **Product Key** to use the [KMS client setup keys][microsoft-kms].

**Need help customizing the configuration files?**

* **Ubuntu Server** - Install and run system-config-kickstart on a Ubuntu desktop.

    ```
    sudo apt-get install system-config-kickstart
    ssh -X rainpole@ubuntu-desktop
    sudo system-config-kickstart
    ```
* **Red Hat Enterprise Linux** (_as well as CentOS Linux/Stream, AlmaLinux, and Rocky Linux_) - Use the [Red Hat Kickstart Generator][redhat-kickstart].
* **Microsoft Windows** - Use the Microsoft Windows [Answer File Generator][microsoft-windows-afg] if you need to customize the provided examples further.

## Build

Start a build by running the build script (`./build_*.sh`).

You can also start a build based on a specific source for some of the virtual machine images.

For example, if you simply want to build a Microsoft Windows Server 2022 Standard Core, run the following:

Initialize the plugins:
```
user@machine > packer init builds/os/ubuntu-20-04
```
Build a machine image:
```
rainpole@macos > packer build -force \
      -var-file="builds/common/vsphere.pkrvars.hcl" \
      -var-file="builds/common/build.pkrvars.hcl" \
      -var-file="builds/common/common.pkrvars.hcl" \
      builds/os/ubuntu-20-04
```
Build a specific machine image using environmental variables:
```
rainpole@macos > packer build -force \
      -var-file="builds/common/common.pkrvars.hcl" \
      builds/os/ubuntu-20-04
```
## Troubleshoot

* Read [Debugging Packer Builds][packer-debug].

[//]: Links
[ansible-docs]: https://docs.ansible.com
[cloud-init]: https://cloudinit.readthedocs.io/en/latest/
[credits-maher-alasfar-twitter]: https://twitter.com/vmwarelab
[credits-maher-alasfar-github]: https://github.com/vmwarelab/cloud-init-scripts
[credits-owen-reynolds-twitter]: https://twitter.com/OVDamn
[credits-owen-reynolds-github]: https://github.com/getvpro/Build-Packer/blob/master/Scripts/Install-VMTools.ps1
[download-git]: https://git-scm.com/downloads
[download-linux-almalinux-server-8]: https://mirrors.almalinux.org/isos.html
[download-linux-centos-server-7]: http://isoredirect.centos.org/centos/7/isos/x86_64/
[download-linux-centos-server-8]: http://isoredirect.centos.org/centos/8/isos/x86_64/
[download-linux-centos-stream-8]: http://isoredirect.centos.org/centos/8-stream/isos/x86_64/
[download-linux-photon-server-4]: https://packages.vmware.com/photon/4.0/
[download-linux-redhat-server-8]: https://access.redhat.com/downloads/content/479/
[download-linux-redhat-server-7]: https://access.redhat.com/downloads/content/69/
[download-linux-rocky-server-8]: https://download.rockylinux.org/pub/rocky/8/isos/x86_64/
[download-linux-ubuntu-server-18-04-lts]: http://cdimage.ubuntu.com/ubuntu/releases/18.04.5/release/
[download-linux-ubuntu-server-20-04-lts]: https://releases.ubuntu.com/20.04.1/
[hashicorp]: https://www.hashicorp.com/
[iso]: https://en.wikipedia.org/wiki/ISO_image
[microsoft-kms]: https://docs.microsoft.com/en-us/windows-server/get-started/kmsclientkeys
[microsoft-windows-afg]: https://www.windowsafg.com
[microsoft-windows-autologon]: https://docs.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/microsoft-windows-shell-setup-autologon-password-value
[microsoft-windows-unattend]: https://docs.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/
[packer]: https://www.packer.io
[packer-debug]: https://www.packer.io/docs/debugging
[packer-install]: https://www.packer.io/intro/getting-started/install.html
[packer-plugin-vsphere]: https://www.packer.io/docs/builders/vsphere/vsphere-iso
[packer-plugin-windows-update]: https://github.com/rgl/packer-plugin-windows-update
[packer-variables]: https://www.packer.io/docs/templates/hcl_templates/variables
[photon-kickstart]: https://vmware.github.io/photon/docs/user-guide/kickstart-through-http/packer-template/
[redhat-kickstart]: https://access.redhat.com/labs/kickstartconfig/
[ssh-keygen]: https://www.ssh.com/ssh/keygen/
[terraform-install]: https://www.terraform.io/docs/cli/install/apt.html
[vmware-pvscsi]: https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.hostclient.doc/GUID-7A595885-3EA5-4F18-A6E7-5952BFC341CC.html
[vmware-vmxnet3]: https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.vm_admin.doc/GUID-AF9E24A8-2CFA-447B-AC83-35D563119667.html
[vsphere-api]: https://code.vmware.com/apis/968
[vsphere-content-library]: https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.vm_admin.doc/GUID-254B2CE8-20A8-43F0-90E8-3F6776C2C896.html
[vsphere-guestosid]: https://vdc-download.vmware.com/vmwb-repository/dcr-public/b50dcbbf-051d-4204-a3e7-e1b618c1e384/538cf2ec-b34f-4bae-a332-3820ef9e7773/vim.vm.GuestOsDescriptor.GuestOsIdentifier.html
[vsphere-efi]: https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.security.doc/GUID-898217D4-689D-4EB5-866C-888353FE241C.html
[vsphere-upload]: https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.storage.doc/GUID-58D77EA5-50D9-4A8E-A15A-D7B3ABA11B87.html?hWord=N4IghgNiBcIK4AcIHswBMAEAzAlhApgM4gC+QA

